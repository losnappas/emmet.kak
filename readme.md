# Emmet.kak

`emmet-wrap .div`

`emmet-wrap-prompt`

You'll need the emmet cli. `npm i -g emmet-cli`

## Install with Plug.kak

```
plug 'https://gitlab.com/losnappas/emmet.kak' %{
	#mappings or whatever.
}
```

### Bonus

```
define-command jsxify-classnames %{
	execute-keys -draft 'sclass=<ret>cclassName=<esc>'
}
define-command ewx -params .. %{
	emmet-wrap %arg{@}
	jsxify-classnames
}
```
