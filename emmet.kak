# We have given up on indenting the output.
# Prettier does not like jsx strings with --parser=html, and we really want those.
# But with --parser=babel it outputs garbage {" "} stuff and whatever.
# Generally just really difficult to make it work correctly. Always some case ruining the day.
# See the commit history for failed attempts.
# Maybe eslint could do better? Dunno.

define-command emmet-wrap -params .. -docstring 'Wrap html' %²
	try %{
		# Align cursors, drop whitespace.
		execute-keys '<a-:>_'
	}
	evaluate-commands -itersel %sh~
		# Whitespace selected? Ignored.
		thing="$(echo "$kak_selection" | wc --words)"
		# Escape quotes that get put to emmet.
		args="$(echo "$@" | sed -e 's/"/\"/g')"

		if [ "$thing" != "0" ];then
			# Escape curlies and dollar signs.
			sel="$(printf "%s\n" "$kak_selection" | sed -e 's/{/\\{/g' -e 's/}/\\}/g' -e 's/\$/\\$/g')"

			# Apply emmet.
			snippet="$(emmet -p "$args{$sel}" 2>&1)"
		else
			snippet="$(emmet "$args" 2>&1)"
		fi
		echo "set-register dquote %¤$snippet¤"
		# Paste it.
		echo 'execute-keys R'
	~
²

# bunch of dupe code, TODO clean it up.
define-command -hidden -params .. emmet-wrap- %²
	evaluate-commands %sh~
		case "$@" in
			'>*'|'')
				exit 1
				;;
		esac
		# Whitespace selected? Ignored.
		thing="$(echo "$kak_selection" | wc --words)"
		# Escape quotes that get put to emmet.
		args="$(echo "$@" | sed -e 's/"/\"/g')"
		sel=""

		if [ "$thing" != "0" ];then
			sel="$(printf "%s\n" "$kak_selection" | sed -e 's/{/\\{/g' -e 's/}/\\}/g' -e 's/\$/\\$/g')"
			# Apply emmet.
			snippet="$(emmet -p "$args{$sel}" 2>&1 )"
		else
			snippet="$(emmet "$args" 2>&1 )"
		fi
		echo "set-register dquote %¤$snippet¤"
		echo 'try %{ execute-keys "<a-;>R" }'
	~
²
define-command emmet-wrap-prompt %<
	try %{
		execute-keys '<a-:>_'
	}
	set-register 'e' %val{selection}
	prompt -on-abort %{
		execute-keys '<a-;>"eR'
	} -on-change %{
		evaluate-commands %{
			execute-keys '<a-;>"eR'
			emmet-wrap- %val(text)
		}
	} 'Emmet:' nop
>

